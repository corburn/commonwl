#!/usr/bin/env cwl-runner

# https://github.com/common-workflow-language/common-workflow-language/issues/170

$namespaces:
  dct: http://purl.org/dc/terms/
  foaf: http://xmlns.com/foaf/0.1/
  doap: http://usefulinc.com/ns/doap#
  adms: http://www.w3.org/ns/adms#
  dcat: http://www.w3.org/ns/dcat#

$schemas:
- http://dublincore.org/2012/06/14/dcterms.rdf
- http://xmlns.com/foaf/spec/20140114.rdf
- http://usefulinc.com/ns/doap#
- http://www.w3.org/ns/adms#
- http://www.w3.org/ns/dcat.rdf

cwlVersion: v1.0
class: Workflow

id: "TODO id"
label: "TODO: label"
doc: |
  TODO: description

#hints:
#  - class: DockerRequirement
#    #dockerPull: "quay.io/corburn/commonwl:master"
#    dockerPull: "quay.io/corburn/commonwl"

#inputs: []

#outputs: []

steps:
  step0:
    run:
      class: CommandLineTool
      baseCommand: [tar, xf]
      in:
        tarfile:
          type: File
          inputBinding:
            position: 1
        extractfile:
          type: string
          inputBinding:
            position: 2
      out:
        example_out:
          type: File
          outputBinding:
            glob: $(inputs.extractfile)

dct:creator:
  "@id": 'https://orcid.org/0000-0003-1546-170X'
  foaf:openid: 'https://orcid.org/0000-0003-1546-170X'
  foaf:name: Jason Travis
  foaf:mbox: 'mailto:jtravis@tgen.org'
#dct:creator:
#- class: foaf:Organization
#  foaf:name: TGen North
#  foaf:member:
#  - class: foaf:Person
#    id: https://orcid.org/0000-0003-1546-170X
#    foaf:openid: https://orcid.org/0000-0003-1546-170X
#    foaf:name: Jason Travis
#    foaf:mbox: mailto:jtravis@tgen.org
