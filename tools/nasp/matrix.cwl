#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
baseCommand:
  - help
inputs:
#--dto-file           Path to the matrix_dto.xml file
#--num-threads        Max number of CPUs that can be executing simultaneously (default: all)
#--reference-fasta    Path to the reference.fasta against which samples are compared
#--reference-dups     Path to the duplicates.txt file marking duplicated positions
#--stats-folder       Path to the output folder for statistics (default: ./)
#--matrix-folder      Path to the output folder for matrices (default: ./)
#--minimum-coverage   Filter positions below this coverage/depth threshold (default: 0)
#--minimum-proportion Filter positions below this proportion threshold (default: 0.0)
#--withallrefpos      Include the withallrefpos.tsv matrix
  - id: minimum-coverage
    doc: Filter positions below this coverage/depth threshold
    default: 10
    type: long
  - id: minimum-proportion
    doc: Filter positions below this proportion threshold
    default: 0.9
    type: float
outputs: []
doc: >-
  TODO
label: nasp matrix
#requirements:
#  - class: ShellCommandRequirement
hints:
  - class: SoftwareRequirement
    packages:
        - package: nasp
  - class: DockerRequirement
    dockerPull: corburn/nasp
$namespaces:
  edam: 'http://edamontology.org/'
  s: 'http://schema.org/'
  sbg: 'https://www.sevenbridges.com/'
$schemas:
  - 'http://edamontology.org/EDAM_1.16.owl'
  - 'https://schema.org/docs/schema_org_rdfa.html'
's:copyrightHolder': TGen North - Translational Genomics Research Institute
