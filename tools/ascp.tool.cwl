#!/usr/bin/env cwl-runner
#
# https://www.commonwl.org/v1.1.0-dev1/cwl.svg

#id:
label: ascp
doc: The IBM Aspera Command-Line Interface (the Aspera CLI) is a collection of Aspera tools for performing high-speed, secure data transfers from the command line. The Aspera CLI is for users and organizations who want to automate their transfer workflows.

################################################################################
# Process
################################################################################
cwlVersion: v1.0
inputs:
    #maxTransferRate:
    #    type: string
    #    default: 200m
    #    doc: 'RATE: G/g(gig),M/m(meg),K/k(kilo)'
    filepath:
        type: string
        #default: anonftp@ftp.ncbi.nlm.nih.gov:${path} .
        inputBinding:
            #prefix: anonftp@ftp.ncbi.nlm.nih.gov:${path} .
            prefix: 'anonftp@ftp.ncbi.nlm.nih.gov:'
            separate: false
            position: 1
    #outdir:
    #    type: Directory
    #    default: ./
    #    inputBinding:
    #        position: 2
outputs:
    info:
        type: stdout
    error: 
        type: stderr
    output:
        type: File
        doc: Response output file
        outputBinding: 
            glob: $(inputs.outName)
#requirements:
#    InlineJavascriptRequirement: {}
hints:
  DockerRequirement:
    #class:
    dockerPull: ibmcom/aspera-cli
    #dockerLoad:
    #dockerFile:
    #dockerImport:
    #dockerImageId:
    #dockerOutputDirectory:
  SoftwareRequirement:
    class: SoftwareRequirement
    packages:
        ascp:
            - https://downloads.asperasoft.com/en/documentation/62
  #NetworkAccess:
  #  class:
  #  networkAccess:
  #ResourceRequirement:
  #  class:
  #  coresMin:
  #  coresMax:
  #  ramMin:
  #  ramMax:
  #  tmpdirMin:
  #  tmpdirMax:
  #  outdirMin:
  #  outdirMax:
  #WorkReuse:
  #  class:
  #  enableReuse:


################################################################################
# CommandLineTool
################################################################################
class: CommandLineTool
#baseCommand:
#    - ascp
#    - -k1
#    - -Tr
#    - -l200m
#    - -i $HOME/.aspera/cli/etc/asperaweb_id_dsa.openssh
baseCommand:
    - echo
#arguments:
#    - privateKeyFile:
#        type: string
#        #default: $HOME/.aspera/cli/etc/asperaweb_id_dsa.openssh
#        default:
#            valueFrom: '"HOME=$HOME"'
#            shellQuote: false
#        inputBinding:
#            prefix: -i
#
#arguments:
#    - prefix: -H
#      valueFrom: |
#          ${
#              return "Authorization: "+inputs.authBearer+" "+inputs.authToken;
#          }
#    - prefix: -F
#      valueFrom: |
#          ${
#              return inputs.label+"=@"+inputs.file.path;
#          }
#stdin: -
#stdout: curl.log
#stderr: curl.error
#successCodes:
#temporaryFailCodes:
#permanentFailCodes:


$namespaces:
 edam: http://edamontology.org/
 s: http://schema.org/
$schemas:
 - http://edamontology.org/EDAM_1.16.owl
 - https://schema.org/docs/schema_org_rdfa.html

#s:license: "https://www.apache.org/licenses/LICENSE-2.0"
s:copyrightHolder: "TGen North - Translational Genomics Research Institute"
